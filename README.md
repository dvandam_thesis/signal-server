Signal-Server
=================

Documentation
-------------

Looking for protocol documentation? Check out the website!

https://signal.org/docs/

Cryptography Notice
------------

This distribution includes cryptographic software. The country in which you currently reside may have restrictions on the import, possession, use, and/or re-export to another country, of encryption software.
BEFORE using any encryption software, please check your country's laws, regulations and policies concerning the import, possession, or use, and re-export of encryption software, to see if this is permitted.
See <http://www.wassenaar.org/> for more information.

The U.S. Government Department of Commerce, Bureau of Industry and Security (BIS), has classified this software as Export Commodity Control Number (ECCN) 5D002.C.1, which includes information security software using or performing cryptographic functions with asymmetric algorithms.
The form and manner of this distribution makes it eligible for export under the License Exception ENC Technology Software Unrestricted (TSU) exception (see the BIS Export Administration Regulations, Section 740.13) for both object code and source code.

Installation Requirements
------------
*  Apache (or other web server)
*  Maven
*  Redis
*  PostgreSQL
*  Coturn
*  Java JDK (Version 8)

Required Services
------------
*  Twilio (Offers free trial)
*  Amazon Web Services S3 (Offers free trial, need a credit card)
*  Firebase Cloud Messaging (Free)

Tested on
------------
* Manjaro Linux 18.0.4 Illyria (Arch Linux based)
* Debian 9.9 Stretch
* Apache and NGINX web server

This guide is written for Arch, for a Debian guide, see: 
[https://gist.github.com/aqnouch/9a371af0614f4fe706a951c2b97651e7]()

AWS S3
------------
1. Create bucket
2. Give name, e.g. 'name.signalserver.attachments'
3. Go to 'My Security Credentials'
4. Click 'Get Started with IAM Users'
5. Click 'Add Users'
6. Add user 'Signal', give it programmatic access
7. Click 'Attach existing policies directly'
8. Check box 'AdministratorAccess'
9. Go to last page and create user
10. Access key ID and secret access key will be used later for the Signal Server 
config
11. Repeat the steps for another bucket, e.g. 'name.signalserver.profiles'

For more help, see:
[https://community.signalusers.org/t/help-with-setting-up-s3-for-profiles-on-signal-server/3297/5]()

Firebase Cloud Messaging
------------
Signal uses the term GCM (Google Cloud Messaging), but this has been deprecated
and the follow-up is called FCM. The API key that we need has been kept the same
by Google.
1. Login to https://console.cloud.google.com
2. Click on 'New project' 
3. Name it, e.g. 'SignalServer'
4. The Project number that appears after creation is your GCM sender ID
5. Go to APIs overview
6. Click 'Enable APIs and services' 
7. Under mobile, click 'cloud messaging' 
8. Click 'create credentials'
9. The API key will be used for the Signal Server config

Signal Server Compilation
------------
1.  `$ mvn clean install -DskipTestsjava`

PostgreSQL Configuration
------------
1. `$ sudo -iu postgres`
2. `[postgres]$ initdb -D /var/lib/postgres/data`
3. `[postgres]$ createdb -U postgres accountsdb`
3. `[postgres]$ createdb -U postgres messagedb`
5. 
```
[postgres]$ createuser --interactive
    Enter name of role to add: signal
    Shall the new role be a superuser? (y/n) y
```
6. `[postgres]$ psql`
7. `[postgres]# ALTER USER signal WITH PASSWORD 'YourPassword';`
8. `[postgres]# exit`
9. `[postgres]$ exit`
10. `$ systemctl start postgresql.service`
11. `$ systemctl enable postgresql.service`

Signal Server Config File
------------
To see an example config file look at `config/sample.yml`.

1. Change Twilio settings findable on the homepage after login
2. Change AWS accessKey and accessSecret (AWS S3 step 10) to your own values
3. Change bucket names and set right region for AWS profiles
4. Change PostgreSQL password to the one just created
5. Change GCM to the one configured with FCM

Apache
------------
We will need a web server that allows secure traffic to route message from the 
Signal client to the Signal server. You could either use a local or public web 
server. We could use a self-signed certificate, create our own root CA or let a 
public CA sign our certificate. See the guide below to set-up your own root CA. 
For the rest of the guide we assume a certificate signed by Let's Encrypt, but
the procedures will almost be the same when using another CA.  
**Note** Import your certificate in the Android client, see:
[https://gitlab.com/dvandam_thesis/signal-android/blob/master/README.md]()
1. In `/etc/httpd/conf/httpd.conf` change the following  
   `ServerName youripaddress` or `ServerName yourdomainname`
2. In `/etc/httpd/conf/httpd.conf` uncomment the following (we need this for 
   proxying and TLS) 
```
LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_http_module modules/mod_porxy_http.so
LoadModule proxy_wstunnel_module module/mod_proxy_wstunnel.so
LoadModule ssl_module modules/mod_ssl.so
LoadModule socache_shmcb_module modules/mod_socache_shmcb.so
Include conf/extra/httpd-ssl.conf
```
3. In `/etc/httpd/conf/extra/httpd-ssl.conf` change the following  
   `ServerName youripaddress` or `ServerName yourdomainname`
4. In `/etc/httpd/conf/extra/httpd-ssl.conf` add the following (to route traffic
   to the Signal Server)  
```
ProxyPreserveHost on
ProxyPass / http://0.0.0.0:8080/
ProxyPassReverse / http://0.0.0.0:8080/
```
5. In `/etc/httpd/conf/extra/httpd-ssl.conf` change the following (change the
   path to the location of your key and certificate, and make sure the private
   key is only readable by the root with `chmod 400 privkey.pem`)  
```
SSLCertificateFile /etc/letsencrypt/live/domain/fullchain.pem  
SSLCertificateKeyFile /etc/letsencrypt/live/domain/privkey.pem
```
6. Start and enable apache  
```
# systemctl start httpd
# systemctl enable httpd
```

For an NGINX guide, see:
[https://community.signalusers.org/t/signal-android-server-ssl-configuration-with-jetty/4432/14]()

Run Signal Server
------------
1. `$ redis-server&`
2. `$ sudo turnserver -a -o -v -n  --no-dtls --no-tls -u test:test -r "someRealm"`
3. `$ java -jar target/TextSecureServer-1.88.jar accountdb migrate
      config/sample.yml`
4. `$ java -jar target/TextSecureServer-1.88.jar messagedb migrate
       config/sample.yml`
5. `$ java -jar target/TextSecureServer-1.88.jar server config/sample.yml`

Create Own Root CA (Optional)
------------
A guide to create your own root CA that can be used with the Apache web server.
These steps are only needed when you don't have a certificate for your web
server yet. This is the easiest solution when your web server is only accessible
from a local network. When hosting on a public domain, please see the guide of
your hosting provider for certificates. When having shell access, you can get a
free certificate from [Let's Encrypt](https://letsencrypt.org/getting-started/).  
**Note** I have not tested this set-up.

**Server Certificate**
1. Generate private key and certificate signing request for the server  
```
# cd /etc/httpd/conf
# openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:4096 -out server.key
# chmod 400 server.key
# openssl req -new -sha256 -key server.key -out server.csr
```
2. Fill in the information needed for the certificate (a lot can be left blank).
   For the CN (Common Name) fill in your local IP address.

**CA Certificate**
1. Generate private key and self-signed certificate for the CA
```
# openssl req -new -x509 -nodes -newkey rsa:4096 -keyout ca.key -out ca.crt -days 365
# chmod 400 ca.key
# openssl req -new -x509 -nodes -key ca.key -sha256 -days 1825 -out ca.pem
```
2. Create certificate for the server signed by the CA  
    `# openssl x509 -req -in server.csr -CA ca.pem -CAkey ca.key -CAcreateserial
       -days 365 -sha256 -out server.crt`

**Note:** View certificates with `openssl x509 -text -noout -in name.crt`  
**Note:** Verify the trust chain with `openssl verify -CAfile ca.pem server.crt`

License
---------------------

Copyright 2013-2016 Open Whisper Systems

Licensed under the AGPLv3: https://www.gnu.org/licenses/agpl-3.0.html
