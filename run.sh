#!/bin/bash

case "$1" in
	build)
		mvn clean install -DskipTestsjava
	;;
	export)
		java -jar target/TextSecureServer-1.88.jar accountdb migrate config/production.yml
		java -jar target/TextSecureServer-1.88.jar messagedb migrate config/production.yml
	;;
	*)
		redis-server&
		java -jar target/TextSecureServer-1.88.jar server config/production.yml
esac
